# eidas-1.4.5

This is a mirror for the official eIDAS Node v1.4.5 available on the [CEF Digital] web site.

[CEF Digital]: https://ec.europa.eu/cefdigital/wiki/pages/viewpage.action?pageId=82772096
